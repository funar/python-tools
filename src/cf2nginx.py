#!/usr/bin/env python3

__author__ = 'Shane DeRidder <shane@deridder.org> <https://gitlab.com/funar/python-tools>'
__copyright__ = 'Copyright (c)2015-2020 Shane DeRidder <shane@deridder.org>'
__license__ = 'GPLv4'
__version__ = '1.0.14'

import sys
import os
import argparse
import certifi
import io
import pycurl
import tempfile

import logging
log = logging.getLogger(__name__)


def main(argv):
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)7s: [%(threadName)s] %(module)s: %(message)s')
    parser = argparse.ArgumentParser()
    pgrp1 = parser.add_mutually_exclusive_group()
    pgrp1.add_argument('--debug', help='Enable debug logging.', default=False, action='store_true')
    pgrp1.add_argument('--info', help='Enable info logging.', default=False, action='store_true')
    pgrp1.add_argument('--warning', help='Enable warning logging.', default=False, action='store_true')
    pgrp1.add_argument('--error', help='Enable error logging.', default=False, action='store_true')
    pgrp1.add_argument('--critical', help='Enable critical logging.', default=False, action='store_true')
    pgrp1.add_argument('--fatal', help='Enable fatal logging.', default=False, action='store_true')
    pgrp1.add_argument('--quiet', help='Completely disable all logging.', default=False, action='store_true')

    parser.add_argument('--config', help='Filename for Nginx server config.', dest='config', default=None, type=str, required=True)
    parser.add_argument('--allow', help='Additional allow rule.', default=list(), type=str, nargs='+')

    try:
        options = parser.parse_args()
    except Exception as e:
        log.fatal('Bad command-line options: %s', str(e))
        sys.exit(-1)

    if options.debug:
        logging.basicConfig(level=logging.DEBUG)
    elif options.info:
        logging.basicConfig(level=logging.INFO)
    elif options.warning:
        logging.basicConfig(level=logging.WARN)
    elif options.error:
        logging.basicConfig(level=logging.ERROR)
    elif options.critical:
        logging.basicConfig(level=logging.CRITICAL)
    elif options.fatal:
        logging.basicConfig(level=logging.FATAL)
    elif options.quiet:
        logging.basicConfig(level=9999)

    log.debug('Getting IPv4 data...')
    try:
        v4_data = get_data('https://www.cloudflare.com/ips-v4')
    except Exception as e:
        log.error('Error: %s', str(e))
        sys.exit(-1)

    log.debug('Getting IPv6 data...')
    try:
        v6_data = get_data('https://www.cloudflare.com/ips-v6')
    except Exception as e:
        log.error('Error: %s', str(e))
        sys.exit(-1)

    temp_config = tempfile.NamedTemporaryFile(delete=False, mode='w')
    log.debug('Creating Config: %s', temp_config.name)
    temp_config.write('# Auto-generated file\n\n')

    for line in v4_data + v6_data:
        temp_config.write('set_real_ip_from {};\n'.format(line))

    temp_config.write('real_ip_header CF-Connecting-IP;\n\n')

    temp_config.write('geo $realip_remote_addr $allowed_ip {\n')
    temp_config.write('    default no;\n\n')

    if options.allow:
        temp_config.write('    # Manual allow rules\n')
        for line in options.allow:
            temp_config.write('    {} yes;\n'.format(line))
        temp_config.write('\n')

    for line in v4_data + v6_data:
        temp_config.write('    {} yes;\n'.format(line))

    temp_config.write('}\n')
    temp_config.close()

    try:
        os.unlink(options.config)
    except FileNotFoundError:
        pass
    except Exception as e:
        log.fatal('Error removing old file: %s', str(e))
        os.unlink(temp_config.name)
        return

    log.debug('Rename: %s -> %s', temp_config.name, options.config)
    os.rename(temp_config.name, options.config)


def get_data(url):
    fd = io.BytesIO()
    curl = pycurl.Curl()
    curl.setopt(curl.URL, url)
    # TODO: Hack! Cloudflare blocks PyCURL...
    curl.setopt(curl.USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36')
    curl.setopt(curl.FOLLOWLOCATION, True)
    curl.setopt(curl.CAINFO, certifi.where())
    curl.setopt(curl.SSL_VERIFYPEER, 1)
    curl.setopt(curl.SSL_VERIFYHOST, 2)
    curl.setopt(curl.WRITEDATA, fd)
    curl.perform()
    curl.close()

    fd.flush()
    fd.seek(0)
    fd_data = fd.read()
    text_data = fd_data.decode('UTF-8')

    text = io.StringIO(text_data)
    lines = text.read().splitlines()
    return lines


if __name__ == '__main__':
    main(sys.argv)
