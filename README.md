### Funar's Python Tools
This is just a collection of Python tools I have written.  Maybe someone else would find them useful.
<https://gitlab.com/funar/python-tools>

All tools released under GPL.

### cf2nginx.py
This simple script downloads and generates Nginx-compatible allow rules for Nginx sites behind Cloudflare.  Useful if your website is still discoverable outside Cloudflare.

Optionally, the --allow flag can be provided to add custom allow rules.